# WestWindUdp

#### 介绍
非常简洁的UDP通信模块, 支持Delphi! 


#### 使用说明

1.  复制WWUdp文件夹到源代码文件夹;
2.  添加WWUdp文件夹的WestWindUdp到工程;
3.  搜索路径添加.\WWUDP
4.  在需要的单元(例如:main.pas)中增加引用 uses WestWindUdp;
5.  在需要的单元(例如:main.pas)中添加UDP接收处理函数function ProcessMsg(AIP,AText :string):Integer;(详见WestWindUdp.pas)
6.  采用wwuInit函数初始化UDP,打开发送和监听端口,并将接收消息匹配到ProcessMsg函数
7.  采用wwuSend函数发送UDP消息
8.  当接收到消息时,自动激活ProcessMsg函数
9.  采用wwuStop函数关闭UDP


#### 参与贡献

1.  欢迎Star!欢迎Watch!欢迎Fork!
2.  欢迎捐赠支持!
3.  欢迎留言交流!



