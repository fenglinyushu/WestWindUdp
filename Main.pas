unit Main;

interface

uses
     //
     WestWindUDP,                 

     //
     XpMan,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    SpinEdit1: TSpinEdit;
    Button_Send: TButton;
    Edit1: TEdit;
    Memo1: TMemo;
    Panel1: TPanel;
    Button_Clear: TButton;
    Button_Init: TButton;
    Button_Stop: TButton;
    procedure Button_ClearClick(Sender: TObject);
    procedure Button_InitClick(Sender: TObject);
    procedure Button_SendClick(Sender: TObject);
    procedure Button_StopClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

function ProcessMsg(AIP,AText :string):Integer;

implementation

{$R *.dfm}

function ProcessMsg(AIP,AText: string): Integer;
begin
     try
          Form1.Memo1.Lines.Add(AIP + ' -> '+AText);
     except
          //
     end;

end;

procedure TForm1.Button_ClearClick(Sender: TObject);
begin
     Memo1.Lines.Clear;
end;

procedure TForm1.Button_InitClick(Sender: TObject);
begin
     wwuInit(SpinEdit1.Value,SpinEdit1.Value,ProcessMsg);
     Button_Init.Enabled := False;
     Button_Stop.Enabled := True;
     Button_Send.Enabled := True;
end;

procedure TForm1.Button_SendClick(Sender: TObject);
begin
     wwuSend(Edit1.Text);
end;

procedure TForm1.Button_StopClick(Sender: TObject);
begin
     wwuStop;
     Button_Init.Enabled := True;
     Button_Stop.Enabled := False;
     Button_Send.Enabled := False;

end;

end.
