object Form1: TForm1
  Left = 217
  Top = 123
  Width = 646
  Height = 563
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Verdana'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    630
    524)
  PixelsPerInch = 96
  TextHeight = 18
  object Button1: TButton
    Left = 15
    Top = 15
    Width = 105
    Height = 28
    Caption = 'Start'
    TabOrder = 0
  end
  object SpinEdit1: TSpinEdit
    Left = 15
    Top = 15
    Width = 121
    Height = 28
    MaxValue = 0
    MinValue = 0
    TabOrder = 1
    Value = 3027
  end
  object Button_Send: TButton
    Left = 15
    Top = 96
    Width = 121
    Height = 26
    Caption = 'Send'
    Enabled = False
    TabOrder = 2
    OnClick = Button_SendClick
  end
  object Edit1: TEdit
    Left = 15
    Top = 56
    Width = 600
    Height = 26
    Anchors = [akLeft, akTop, akRight]
    ImeName = #20013#25991'('#31616#20307') - '#26497#28857#20116#31508
    TabOrder = 3
    Text = 'Edit1'
  end
  object Memo1: TMemo
    Left = 15
    Top = 216
    Width = 600
    Height = 297
    Anchors = [akLeft, akTop, akRight, akBottom]
    ImeName = #20013#25991'('#31616#20307') - '#26497#28857#20116#31508
    Lines.Strings = (
      'Memo1')
    TabOrder = 4
  end
  object Panel1: TPanel
    Left = 15
    Top = 188
    Width = 600
    Height = 28
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    Caption = 'Received Messages'
    TabOrder = 5
  end
  object Button_Clear: TButton
    Left = 15
    Top = 162
    Width = 121
    Height = 26
    Caption = 'Clear'
    TabOrder = 6
    OnClick = Button_ClearClick
  end
  object Button_Init: TButton
    Left = 143
    Top = 16
    Width = 121
    Height = 26
    Caption = 'Init'
    TabOrder = 7
    OnClick = Button_InitClick
  end
  object Button_Stop: TButton
    Left = 271
    Top = 16
    Width = 121
    Height = 26
    Caption = 'Stop'
    Enabled = False
    TabOrder = 8
    OnClick = Button_StopClick
  end
end
